﻿using System.Text.Json.Serialization;

namespace Domain;

public class CategoryModel : BaseModel
{
    public string Name { get; set; } 

    public List<PropertyModel> Properties { get; set; }
}
