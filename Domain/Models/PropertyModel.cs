﻿namespace Domain;

public class PropertyModel : BaseModel
{
    public string Name { get; set; }
    public Guid CategoryId { get; set;}
}
