﻿namespace Domain;

public class CategoryQuery : BaseQuery
{
    public string? Name { get; set; }
}
