﻿namespace Domain;

public class PropertyFilter
{
    public Guid? PropertyId { get; set; }
    public string Value { get; set; }
}

public class ProductQuery : BaseQuery
{
    public List<PropertyFilter> PropertyFilters { get; set; } = new();
}
