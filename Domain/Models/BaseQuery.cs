﻿namespace Domain;

public class BaseQuery
{
    public Guid? Id { get; set; }
    public int Skip { get; set; }
    public int Take { get; set; } = 50;
}
