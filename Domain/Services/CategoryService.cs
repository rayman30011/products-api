﻿using AutoMapper;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Domain;

public class CategoryService : CrudService<CategoryModel, Category, CategoryQuery>
{
    public CategoryService(ProductsDbContext context, IMapper mapper) : base(context, mapper)
    {
    }

    protected override IQueryable<Category> ProccessSerach(IQueryable<Category> dbQuery, CategoryQuery query)
    {
        dbQuery = dbQuery.Include(x => x.Properties);
        
        if (!string.IsNullOrWhiteSpace(query.Name))
            dbQuery = dbQuery.Where(x => query.Name.Contains(x.Name));
            
        return dbQuery;
    }

    protected override Task Update(CategoryModel model, Category entity)
    {   
        entity.Name = model.Name;

        return Task.CompletedTask;
    }
}
