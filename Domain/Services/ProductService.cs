﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Domain;

public class ProductService : CrudService<ProductModel, Product, ProductQuery>, ICrudService<ProductModel, ProductQuery>
{
    private readonly PropertyService _propertyService;

    public ProductService(ProductsDbContext context, IMapper mapper, PropertyService propertyService) : base(context, mapper)
    {
        _propertyService = propertyService;
    }

    protected override IQueryable<Product> ProccessSerach(IQueryable<Product> dbQuery, ProductQuery query)
    {
        dbQuery = dbQuery.Include(x => x.Values);

        if (query.PropertyFilters.Any())
        {
            foreach (var filter in query.PropertyFilters) 
            {
                dbQuery = dbQuery.Where(x => x.Values.Any(v => v.PropertyId == filter.PropertyId && v.Value == filter.Value));
            }
        }

        var a = dbQuery.ProjectTo<ProductModel>(_mapper.ConfigurationProvider).ToList();

        return dbQuery;
    }

    protected override async Task Update(ProductModel model, Product entity)
    {
        entity.Name = model.Name;
        entity.Price = model.Price;
        entity.CategoryId = model.CategoryId;
        entity.Description = model.Description;

        var categoryPropertyIds = (await _propertyService.Search(new() { CategoryId = model.CategoryId }))
            .Select(x => x.Id).ToHashSet();

        if (!model.Properties.All(x => categoryPropertyIds.Contains(x.Key)))
            throw new Exception("Свойство не содержится в данной категории");

        entity.Values = model.Properties.Select(x => new PropertyValue 
        {
            ProductId = entity.Id,
            PropertyId = x.Key,
            Value = x.Value
        }).ToList();
    }
}
