﻿using AutoMapper;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Domain;

public class PropertyService : CrudService<PropertyModel, Property, PropertyQuery>, ICrudService<PropertyModel, PropertyQuery>
{
    public PropertyService(ProductsDbContext context, IMapper mapper) : base(context, mapper)
    {
    }

    protected override IQueryable<Property> ProccessSerach(IQueryable<Property> dbQuery, PropertyQuery query)
    {
        dbQuery = dbQuery.Where(x => !x.Category.Deleted);

        if (query.CategoryId.HasValue)
            dbQuery = dbQuery.Where(x => x.CategoryId == query.CategoryId);
            
        return dbQuery;
    }

    protected override Task Update(PropertyModel model, Property entity)
    {
        entity.Name = model.Name;
        entity.CategoryId = model.CategoryId;
        
        return Task.CompletedTask;
    }
}
