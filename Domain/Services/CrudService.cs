﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Domain;

public abstract class CrudService<TModel, TEntity, TQuery> : ICrudService<TModel, TQuery>
    where TModel : BaseModel
    where TEntity : BaseEntity, new()
    where TQuery : BaseQuery
{
    protected readonly ProductsDbContext _context;
    protected readonly IMapper _mapper;

    public CrudService(ProductsDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<Guid> Create(TModel model)
    {
        var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == model.Id);
        bool isAdd = false;
        if (entity == null)
        {
            entity = new TEntity { Id = Guid.NewGuid() };
            isAdd = true;
        }

        await Update(model, entity);
        
        if (isAdd)
            _context.Set<TEntity>().Add(entity);

        await _context.SaveChangesAsync();
        return entity.Id;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
        if (entity == null)
            throw new Exception("Запись не найдена");

        entity.Deleted = true;
        await _context.SaveChangesAsync();
    }

    public Task<List<TModel>> Search(TQuery query)
    {
        var dbQuery = _context.Set<TEntity>()
            .Where(x => !x.Deleted)
            .AsNoTracking();

        if (query.Id.HasValue)
            dbQuery = dbQuery.Where(x => x.Id == query.Id);

        dbQuery = ProccessSerach(dbQuery, query);

        return dbQuery
            .Skip(query.Skip)
            .Take(query.Take)
            .ProjectTo<TModel>(_mapper.ConfigurationProvider)
            .ToListAsync();
    }

    protected abstract IQueryable<TEntity> ProccessSerach(IQueryable<TEntity> dbQuery, TQuery query);

    protected abstract Task Update(TModel model, TEntity entity);
}
