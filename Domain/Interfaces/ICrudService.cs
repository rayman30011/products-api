﻿using DataAccess;

namespace Domain;

public interface ICrudService<TModel, TQuery>
    where TModel : class
    where TQuery : class
{
    Task<Guid> Create(TModel model);
    Task<List<TModel>> Search(TQuery query);
    Task Delete(Guid id);
}
