﻿using AutoMapper;
using DataAccess;

namespace Domain;

public class CategoryProfile : Profile
{
    public CategoryProfile()
    {
        CreateMap<Property, PropertyModel>();
        CreateMap<Category, CategoryModel>();
    }
}
