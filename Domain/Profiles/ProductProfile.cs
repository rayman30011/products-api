﻿using AutoMapper;
using DataAccess;

namespace Domain;

public class ProductProfile : Profile
{
    public ProductProfile()
    {
        CreateMap<PropertyValue, KeyValuePair<Guid, string>>()
            .ConstructUsing(x => new KeyValuePair<Guid, string>(x.PropertyId, x.Value));

        CreateMap<Product, ProductModel>()
            .ForMember(x => x.Properties, o => o.MapFrom(src => src.Values));
    }
}
