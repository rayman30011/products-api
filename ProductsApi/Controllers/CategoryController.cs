﻿using Domain;
using Microsoft.AspNetCore.Mvc;

namespace ProductsApi;

[ApiController]
[Route("api/[controller]")]
public class CategoryController : CrudController<CategoryModel, CategoryQuery>
{
    public CategoryController(CategoryService service) : base(service)
    {
    }
}
