﻿using Domain;
using Microsoft.AspNetCore.Mvc;

namespace ProductsApi;

public class CrudController<TModel, TQuery> : ControllerBase
    where TModel : class
    where TQuery : class
{
    protected readonly ICrudService<TModel, TQuery> _service;

    public CrudController(ICrudService<TModel, TQuery> service)
    {
        _service = service;
    }

    [HttpPost("search")]
    public Task<List<TModel>> Search([FromBody] TQuery query) => _service.Search(query);

    [HttpPost]
    public Task<Guid> Create([FromBody] TModel model) => _service.Create(model);

    [HttpDelete("{id}")]
    public Task Delete(Guid id) => _service.Delete(id);
}
