﻿using Domain;
using Microsoft.AspNetCore.Mvc;

namespace ProductsApi;

[ApiController]
[Route("api/[controller]")]
public class ProductController : CrudController<ProductModel, ProductQuery>
{
    public ProductController(ProductService service) : base(service)
    {
    }
}
