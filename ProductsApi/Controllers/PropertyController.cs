﻿using Domain;
using Microsoft.AspNetCore.Mvc;

namespace ProductsApi;

[ApiController]
[Route("api/[controller]")]
public class PropertyController : CrudController<PropertyModel, PropertyQuery>
{
    public PropertyController(PropertyService service) : base(service)
    {
    }
}
