﻿namespace DataAccess;

public class BaseEntity
{
    public Guid Id { get; set; }
    public bool Deleted { get; set; }
}
