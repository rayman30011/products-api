﻿namespace DataAccess;

public class Product : BaseEntity
{
    public string Name { get; set; }
    public string Description { get; set; }

    public decimal Price { get; set; }

    public Guid CategoryId { get; set; }
    public Category? Category { get; set; }

    public List<PropertyValue> Values { get; set; } = new();
}
