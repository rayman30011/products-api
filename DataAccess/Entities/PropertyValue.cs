﻿namespace DataAccess;

public class PropertyValue
{
    public Guid ProductId { get; set; }
    public Product? Product { get; set; }

    public Guid PropertyId { get; set; }
    public Property? Property { get; set; }

    public string Value { get; set; }
}
