﻿namespace DataAccess;

public class Property : BaseEntity
{
    public string Name { get; set; }

    public Guid CategoryId { get; set; }
    public Category? Category { get; set; }
}
