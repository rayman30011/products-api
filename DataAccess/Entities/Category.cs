﻿namespace DataAccess;

public class Category : BaseEntity
{
    public string Name { get; set; }

    public List<Property> Properties { get; set; } = new();
}
