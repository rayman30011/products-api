﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace DataAccess;

public class ProductsDbContext : DbContext
{
    ProductsDbContext() : base() {}

    public ProductsDbContext(DbContextOptions<ProductsDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProductsDbContext).Assembly);
    }
}
