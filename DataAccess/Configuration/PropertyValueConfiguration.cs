﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess;

public class PropertyValueConfiguration : IEntityTypeConfiguration<PropertyValue>
{
    public void Configure(EntityTypeBuilder<PropertyValue> builder)
    {
        builder.HasKey(x => new { x.PropertyId, x.ProductId });

        builder.HasOne(x => x.Product).WithMany(x => x.Values).HasForeignKey(x => x.ProductId);
        builder.HasOne(x => x.Property).WithMany().HasForeignKey(x => x.PropertyId);
    }
}
